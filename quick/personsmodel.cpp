#include "personsmodel.h"

PersonsModel::PersonsModel(QObject *parent) : QAbstractTableModel(parent)
{
}

int PersonsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_persons.count();
}

int PersonsModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return LAST;
}

QVariant PersonsModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid() || m_persons.count() <= index.row() || (
                role != Qt::DisplayRole && role != Qt::EditRole)) {
        return QVariant();
    }
    return m_persons[index.row()][ Column(index.column())];
}

bool PersonsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid() || role != Qt::EditRole || m_persons.count() <= index.row()) {
        return false;
    }
    m_persons[index.row()][Column(index.column())] = value;
    emit dataChanged(index, index);
    return true;
}

QVariant PersonsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole) {
        return QVariant();
    }
    if(orientation == Qt::Vertical) {
        return section;
    }
    switch(section) {
    case SURNAME:
        return tr("Surname");
    case NAME:
        return tr("Name");
    case PATRONYMIC:
        return tr("Patronymic");
    case SELECTION:
        return tr("Selection");
    }
    return QVariant();
}

Qt::ItemFlags PersonsModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if(index.isValid()) {
        if(index.column() == SELECTION) {
            flags |= Qt::ItemIsEditable;
        }
    }
    return flags;
}

void PersonsModel::appendPerson(const QString &surname, const QString &name, const QString &patronymic)
{
    PersonData person;
    person[SURNAME] = surname;
    person[NAME] = name;
    person[PATRONYMIC] = patronymic;
    person[SELECTION] = false;
    int row = m_persons.count();
    beginInsertRows(QModelIndex(), row, row);
    m_persons.append(person);
    endInsertRows();
}

void PersonsModel::removeSelected()
{
    int i = 0;
    Persons::iterator it = m_persons.begin();
    while(it != m_persons.end()) {
        if(it->value(SELECTION, false).toBool()) {
            beginRemoveRows(QModelIndex(), i, i);
            it = m_persons.erase(it);
            endRemoveRows();
        } else {
            ++i;
            ++it;
        }
        if(it == m_persons.end()) {
            break;
        }
    }
}
