<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>DemoExample</source>
        <translation>ДемоПример</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="28"/>
        <source>Tab 1</source>
        <translation>Вкладка 1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="59"/>
        <source>Surname</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="82"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="105"/>
        <source>Patronymic</source>
        <translation>Отчество</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="47"/>
        <source>&lt;a href=&quot;#&quot;&gt;Remove&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;#&quot;&gt;Удалить&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>Tab 2</source>
        <translation>Вкладка 2</translation>
    </message>
</context>
<context>
    <name>PersonsModel</name>
    <message>
        <location filename="personsmodel.cpp" line="48"/>
        <source>Surname</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <location filename="personsmodel.cpp" line="50"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="personsmodel.cpp" line="52"/>
        <source>Patronymic</source>
        <translation>Отчество</translation>
    </message>
    <message>
        <location filename="personsmodel.cpp" line="54"/>
        <source>Selection</source>
        <translation>Выбор</translation>
    </message>
</context>
</TS>
