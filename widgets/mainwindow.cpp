#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QSortFilterProxyModel *sortModel= new  QSortFilterProxyModel(this);
    m_model = new PersonsModel(this);
    sortModel->setSourceModel(m_model);
    ui->mainTableView->setModel(sortModel);

    connect(ui->removeSelectedLabel, &QLabel::linkActivated, m_model, &PersonsModel::removeSelected);
    connect(ui->filterLineEdit, &QLineEdit::textEdited, sortModel, &QSortFilterProxyModel::setFilterFixedString);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_addPushButton_clicked()
{
    m_model->appendPerson(ui->surnameLineEdit->text(),
                          ui->nameLineEdit->text(),
                          ui->patronymicLineEdit->text());
}


