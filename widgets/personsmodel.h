#ifndef PERSONSMODEL_H
#define PERSONSMODEL_H

#include <QAbstractTableModel>

class PersonsModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit PersonsModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex& parent) const;
    int columnCount(const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    bool setData(const QModelIndex& index, const QVariant& value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex& index) const;
    void appendPerson(const QString& surname, const QString& name, const QString& patronymic);

public slots:
    void removeSelected();

private:
    enum Column {
        SURNAME = 0,
        NAME,
        PATRONYMIC,
        SELECTION,
        LAST
    };
    typedef QHash<Column, QVariant> PersonData;
    typedef QList<PersonData> Persons;
    Persons m_persons;
};

#endif // PERSONSMODEL_H
